// 定义依赖项
var gulp = require('gulp'),
    connect = require('gulp-connect'),
    less = require('gulp-less'),
    watch = require('gulp-watch');

// 定义 webserver 任务
gulp.task('webserver', function() {
    connect.server({
        livereload: true
    });
});

// 定义 less 任务
gulp.task('less', function() {
    gulp.src('css/*.less')
        .pipe(less())
        .pipe(gulp.dest('css'))
        .pipe(connect.reload());
});

// 定义 watch 任务
gulp.task('watch', function() {
    gulp.watch('css/**/*.less', ['less']);
})

// 定义默认任务
gulp.task('default', ['less', 'webserver', 'watch']);
