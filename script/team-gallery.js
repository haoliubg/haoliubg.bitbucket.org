(function($) {
    $.fn.teamgallery = function(options) {
        // This is the easiest way to have default options.
        var settings = $.extend({
            dataURL: "/script/team-info.json"
        }, options);

        var self = this;
        var dataset = [];
        var avatars = [];
        var yOrigin = null;
        var lastTop = null;
        var screen_sm = 768;
        var screen_md = 1024;
        var screen_lg = 1450;

        var Spotlight = {
            createNew: function() {
                var elem = $('<div class="spotlight"></div>');
                elem.left = $('<div class="left col-md-4"></div>');
                elem.image = $('<img src="" class="img-responsive" />');
                elem.right = $('<div class="right col-md-8"></div>');
                elem.info = $('<div class="info"></div>');
                elem.close = $('<div class="btn close-btn"></div>');
                elem.showid = null;
                elem.close.on('click', function() {
                    elem.showid = null;
                    elem.velocity("fadeOut", {
                        duration: 300,
                        complete: function() {
                            elem.info.empty();
                        }
                    })
                    self.resize(avatars);
                });

                elem.append(elem.left);
                elem.append(elem.right);
                elem.append(elem.close);
                elem.left.append(elem.image);
                elem.right.append(elem.info);

                // methods
                elem.init = function() {}
                elem.setShowid = function(id) {
                    elem.showid = id;
                }
                elem.setSize = function(globalWidth) {
                    var width = $(window).width();
                    if (width >= screen_md) { // landscape and widescreen
                        elem.css({
                            "width": globalWidth,
                            "height": globalWidth / 3
                        });
                    } else if (width < screen_sm) { // mobile
                        elem.css({
                            "width": globalWidth,
                            "height": globalWidth * 2
                        });
                    } else if (width >= screen_sm && width < screen_md) {
                        elem.css({
                            "width": globalWidth,
                            "height": globalWidth * 1.2
                        });
                    }
                }
                elem.setTop = function(top) {
                    elem.css("top", top);
                    elem.yOrigin = elem.offset().top + top - elem.lastTop - 40;
                    elem.lastTop = top;
                }
                elem.setImage = function(imageURL) {
                    elem.image.attr("src", imageURL);
                }
                elem.setInfo = function(data, type) { // type = 0 mgmt, 1 staff

                    var name = $('<p class="name">' + data['name'] + '</p>');
                    var position = $('<p class="position">' + data['position'] + '</p>');
                    var social = $('<div class="social"></div>');
                    for (var i = 0; i < data['social'].length; i++) {
                        var item = $('<a target="_blank" href="' + data['social'][i]['URL'] + '"><img class="social-icon" src="/img/social_icon/' + data['social'][i]['icon'] + '.svg"></a>');
                        social.append(item);
                    }
                    elem.info.hide();
                    elem.info.append(name).append(position).append(social);

                    if (type === 0) { // mgmt
                        var description = $('<p class="description"></p>');
                        description.text(data['description']);
                        elem.info.append(description);
                    } else if (type === 1) { // staff
                        var browserHistory = $('<p class="browser-history">browser history</p>');
                        var interest = $('<div class="interest"></div>');
                        for (var i = 0; i < data['interest'].length; i++) {
                            var item = $('<a target="_blank" class="btn item" href="' + data['interest'][i]['URL'] + '">' + data['interest'][i]['name'] + '</a>');
                            if (i >= 6) {
                                item.addClass("hidden-xs");
                            }
                            if (i >= 5) {
                                //item.addClass("hidden-md");
                            }
                            interest.append(item);
                            elem.info.append(browserHistory).append(interest);
                        }
                    }
                    elem.info.show();
                }
                elem.clearInfo = function(data) {
                    elem.info.empty();
                }

                return elem;
            }
        }

        var s = Spotlight.createNew();
        s.init();

        var Avatar = {
            createNew: function() {
                var elem = $('<div class="avatar"></div>');

                // init
                elem.uid = null;
                elem.info = null;
                elem.img = $('<img src="" class="img-responsive" />');

                // methods
                elem.init = function(key) {
                    elem.uid = key;
                    elem.append(elem.img);
                }
                elem.setSize = function(globalWidth) {
                    var width = $(window).width();
                    if (width >= screen_md) { // desktop
                        elem.css({
                            "width": globalWidth / 6,
                            "height": globalWidth / 6
                        });
                    } else if (width < screen_sm) {
                        if (elem.uid === 0 || elem.uid === 1) { // mobile first 2
                            elem.css({
                                "width": globalWidth / 2,
                                "height": globalWidth / 2
                            })
                        } else { // mobile later
                            elem.css({
                                "width": globalWidth / 3,
                                "height": globalWidth / 3
                            });
                        }
                    } else if (width >= screen_sm && width < screen_md) { // portrait
                        elem.css({
                            "width": globalWidth / 5,
                            "height": globalWidth / 5
                        });
                    }
                }
                elem.place = function(globalWidth, showid) {
                    var width = $(window).width();
                    if (width >= screen_md) { // > landscape
                        if (s.showid !== null && elem.uid >= (Math.floor(showid / 6) + 1) * 6) {
                            elem.velocity({
                                "top": Math.floor(elem.uid / 6) * globalWidth / 6 + globalWidth / 3,
                                "left": elem.uid % 6 * globalWidth / 6
                            }, 500);
                        } else {
                            elem.velocity({
                                "top": Math.floor(elem.uid / 6) * globalWidth / 6,
                                "left": elem.uid % 6 * globalWidth / 6,
                            }, 500);
                        }
                    } else if (width < screen_sm) { // mobile
                        if (elem.uid === 0 || elem.uid === 1) {
                            elem.velocity({
                                "top": 0,
                                "left": Math.floor(elem.uid / 1) * globalWidth / 2
                            }, 500);
                        } else {
                            if (s.showid !== null && elem.uid >= ((Math.floor((showid - 2) / 3 + 1) * 3) + 2)) { // -2 because of top 2
                                elem.velocity({
                                    "top": Math.floor((elem.uid - 2) / 3) * globalWidth / 3 + globalWidth / 2 + globalWidth * 2,
                                    "left": (elem.uid - 2) % 3 * globalWidth / 3
                                }, 500);
                            } else {
                                elem.velocity({
                                    "top": Math.floor((elem.uid - 2) / 3) * globalWidth / 3 + globalWidth / 2,
                                    "left": (elem.uid - 2) % 3 * globalWidth / 3
                                }, 500);
                            }
                        }
                    } else if (width >= screen_sm && width < screen_md) { // portrait
                        if (s.showid !== null && elem.uid >= (Math.floor(showid / 5) + 1) * 5) {
                            elem.velocity({
                                "top": Math.floor(elem.uid / 5) * globalWidth / 5 + globalWidth * 1.2,
                                "left": elem.uid % 5 * globalWidth / 5
                            }, 500);
                        } else {
                            elem.velocity({
                                "top": Math.floor(elem.uid / 5) * globalWidth / 5,
                                "left": elem.uid % 5 * globalWidth / 5,
                            }, 500);
                        }
                    }
                }
                elem.setImage = function(imageURL) {
                    elem.img.attr("src", imageURL);
                }
                elem.on('click', function() {
                    // reset spotlight
                    s.setShowid(elem.uid);
                    // if s is shown then fade out it first

                    s.velocity("fadeOut", {
                        duration: 200,
                        complete: function() {
                            s.clearInfo();
                            s.setImage(elem.img.attr("src"));
                            if (s.showid == 1 || s.showid == 0) {
                                s.setInfo(elem.info, 0);
                            } else {
                                s.setInfo(elem.info, 1);
                            }
                            s.velocity("fadeIn", {
                                duration: 200
                            });
                        }
                    });

                    // other items
                    self.resize(avatars);

                    s.velocity("scroll", {
                        duration: 500,
                        offset: s.offset.top + 60,
                        queue: false
                    });
                });

                return elem;
            }
        }

        self.setHeight = function(globalWidth, num) {
            var width = $(window).width();
            var height = 0;
            if (width >= screen_md) { // screen > landscape
                if (s.showid === null) {
                    height = globalWidth / 6 * Math.ceil(num / 6);
                } else {
                    height = globalWidth / 6 * Math.ceil(num / 6) + globalWidth / 3;
                }
            } else if (width < screen_sm) { // sm device
                if (s.showid === null) {
                    height = globalWidth / 3 * Math.ceil((num - 2) / 3) + globalWidth / 2;
                } else {
                    height = globalWidth / 3 * Math.ceil((num - 2) / 3) + globalWidth / 2 + globalWidth * 2;
                }
            } else if (width >= screen_sm && width < screen_md) { // portrait
                if (s.showid === null) {
                    height = globalWidth / 5 * Math.ceil(num / 5);
                } else {
                    height = globalWidth / 5 * Math.ceil(num / 5) + globalWidth * 1.2;
                }
            }
            self.css("height", height);
        }
        self.init = function(data) {
            self.css("position", "relative");
            self.setHeight(self.width(), data.length);

            // init gallery
            $.each(data, function(key, val) {
                var a = Avatar.createNew();
                a.init(key);
                a.info = val;
                a.setSize(self.width());
                a.place(self.width());
                a.setImage(val["avatar"]);
                self.append(a);
                avatars.push(a);
            });

            s.setSize(self.width());
            self.append(s);
            s.hide();
        }
        self.resize = function(data) {
            self.setHeight(self.width(), data.length);
            var width = $(window).width();
            // spotlight replace & resize
            if (width >= screen_md) { // > landscape
                s.setSize(self.width());
                s.setTop(self.width() / 6 * (Math.floor(s.showid / 6) + 1));
            } else if (width < screen_sm) { // mobile
                s.setSize(self.width());
                s.setTop(self.width() / 2 + self.width() / 3 * (Math.floor((s.showid - 2) / 3) + 1))
            } else if (width >= screen_sm && width < screen_md) { // portrait
                s.setSize(self.width());
                s.setTop(self.width() / 5 * (Math.floor(s.showid / 5) + 1));
            }

            $.each(data, function(key, val) {
                val.setSize(self.width());
                val.place(self.width(), s.showid);
            });
        }

        $.getJSON(settings.dataURL, function(data) {
            dataset = data;
            self.init(data);
        });

        var resizeTimeout;
        $(window).resize(function() {
            console.log($(window).width());
            if (!!resizeTimeout) {
                clearTimeout(resizeTimeout);
            }
            resizeTimeout = setTimeout(function() {
                self.resize(avatars);
            }, 200);

        });

        return this;
    };
}(jQuery));
