$(function() {
    // keep window height
    var resizeTimeout; // use timeout to increase performance
    $(".window-height").height($(window).height());
    $(window).resize(function() {
        if (!!resizeTimeout) {
            clearTimeout(resizeTimeout);
        }
        resizeTimeout = setTimeout(function() {
            $(".window-height").height($(window).height());
        }, 200);

    });

    // scroll to story
    $(".scroll-down").on('click', function() {
        $("html, body").animate({
            scrollTop: $('#story').offset().top
        }, 500);
    });

    // show full video
    $('.play-button').on('click', function() {
        $('#mission .overlay').animate({
            "background-color": "rgba(22, 33, 63, 1)"
        }, 200);
        $('#video-full').toggle();
        $('#video-full iframe').attr("src", $(this).data("url"));
    });
    $('#video-full button.close').on('click', function() {
        $('#video-full').toggle();
        $('#video-full iframe').attr("src", "");
    });

    // scroll window
    $(window).scroll(function() {
        //clearTimeout($.data(this, 'scrollTimer'));
        //$.data(this, 'scrollTimer', setTimeout(function() { // used to determine if scrolling stopped
        var yOrigin = $(this).scrollTop();
        var winHeight = $(window).height();
        var secHeight = 500; // extra height scrolling down to hide first nav
        if (yOrigin >= winHeight && yOrigin < winHeight + secHeight) {
            $('#navbar').removeClass("not-show");
            $('#navbar').removeClass("half-show");
            $('#navbar').addClass("full-show");

            $('.navbar-bottom .logo').removeClass('show');
        } else if (yOrigin < winHeight) {
            $('#navbar').removeClass("half-show");
            $('#navbar').removeClass("full-show");
            $('#navbar').addClass("not-show");

            $('.navbar-bottom .logo').removeClass('show');
        } else if (yOrigin >= winHeight + secHeight) {
            $('#navbar').removeClass("full-show");
            $('#navbar').removeClass("not-show");
            $('#navbar').addClass("half-show");

            $('.navbar-bottom .logo').addClass('show');
        };
        //}, 250));
    });

    // dummy count val
    var countingVal = 72727167123;
    setInterval(function() {
        countingVal = parseInt(countingVal) + Math.floor(Math.random() * 8);
        $("#count .count .val").text(commaSeparateNumber(countingVal));
    }, 1000);
    // end count val

});

// absolute position div set "top: ??px"
function slideTopAnimate(elem, top) {
    elem.animate({
        top: top
    }, {
        duration: 300,
        queue: false
    });
}

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}
